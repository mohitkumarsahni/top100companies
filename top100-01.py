def gen(lst):
    for i in lst:
        yield i

from bs4 import BeautifulSoup
import requests
import pandas as pd
import time
from sys import exit

f = open('/media/mohitsahni/DATAPOOL-150/QunattaAnalytics/project.02.Nov-11/log.txt','a')
f.write('Started at : '+time.asctime( time.localtime(time.time()) )+'\n')

mainUrl = "http://www.moneycontrol.com"     #main url of the website.

startUrl = "http://www.moneycontrol.com/stocks/marketinfo/marketcap/bse/index.html"     #start url for top 100 companies.

cat = None # for companies catagory.
comp_rank = 0   #for rank number of companies useless after 100.
comp_list = []  #for companies info to be stored.

try:
    main_html = requests.get(startUrl)  # page of starting url.
except:
    f.write('Finished  at : '+time.asctime( time.localtime(time.time()) )+' : Error in the Starting Page.')
    exit()

try:
    main_text = BeautifulSoup(main_html.text,'html.parser')
    main_p = main_text.find_all('p',class_='FL PL15')   #finding the list of all catagories.
except:
    f.write('Finished  at : '+time.asctime( time.localtime(time.time()) )+' : Error in Finding Main List from start page.')
    exit()

for i in main_p:
    for j in i.find_all('option'):
        if j['value'] == '':
            cat = 'Top 100'
            td_comp = main_text.find_all('td', {'width':'25%'});
            for k in td_comp:
                comp_info = {} #for company info.
                temp = [] #for address.
                a = k.find('a')
                comp_rank = comp_rank + 1
                comp_info['rank'] = comp_rank
                comp_info['name'] = a.text
                comp_info['catagory'] = cat
                #print comp_info
                html_inlink = requests.get(mainUrl+a['href'])       #going into the company_info button link.
                text_inlink = BeautifulSoup(html_inlink.text,'html.parser')
                dt = text_inlink.find('dt', string='COMPANY INFO')      #finding the sublink.
                a_in = dt.find('a')
                #print "Going into URL:"+mainUrl+a_in['href']
                comp_facts = requests.get(mainUrl+a_in['href'])         #going into the sublink.
                comp_facts_text = BeautifulSoup(comp_facts.text,'html.parser')
                div = comp_facts_text.find('div',class_='FL b-12 PA5')
                #print div
                try:
                    div_lst = div.text.split('\n')
                except:
                    comp_list.append(comp_info)     #because giving error on rank = 54.
                    continue
                #print div_lst
                #print div_lst[1]
                for z in div_lst[2:]:
                    if(z.replace('\n','').replace('\t','') == ''):
                        continue
                    else:
                        #print z.replace('\n','').replace('\t','')
                        if(z.replace('\n','').replace('\t','').find('Tel: ') != -1):
                            comp_info['telephone'] = z.replace('\n','').replace('\t','').replace('Tel: ','')
                        elif(z.replace('\n','').replace('\t','').find('Fax:') != -1):
                            comp_info['fax'] = z.replace('\n','').replace('\t','').replace('Fax: ','')
                        elif(z.replace('\n','').replace('\t','').find('Email:') != -1):
                            comp_info['email'] = z.replace('\n','').replace('\t','').replace('Email: ','')
                        elif(z.replace('\n','').replace('\t','').find('Website:') != -1):
                            comp_info['website'] = z.replace('\n','').replace('\t','').replace('Website: ','')
                        elif(z.replace('\n','').replace('\t','').find('Group:') != -1):
                            comp_info['group'] = z.replace('\n','').replace('\t','').replace('Group: ','')
                        else:
                            if(z.replace('\n','').replace('\t','').find(',') == -1):
                                temp_coma = None
                                temp_coma = z.replace('\n','').replace('\t','')
                                coma = temp_coma+','
                                temp.append(coma)
                            else:
                                temp.append(z.replace('\n','').replace('\t',''))
                comp_info['registered address'] = ''.join(temp)

                table4 = comp_facts_text.find('div',class_='boxBg1 table4')
                table_lst = []
                for k in table4.find_all('tr'):
                    for l in k.find_all('td',class_='b-12 PT5'):
                            table_lst.append(l.text)
                temp_gen = gen(table_lst)
                while True:
                    try:
                        value = temp_gen.next()
                        key = temp_gen.next()
                        if key in comp_info.keys():
                            comp_info[key].append(value)
                        else:
                            comp_info[key]=[value]
                    except:
                        break
                for k in comp_info.keys():
                    if isinstance(comp_info[k],list):
                        value_str = ','.join(comp_info[k])
                        comp_info[k]=value_str
                f.write('Done for : '+str(comp_rank))       #writing to log file.
                print comp_info
                comp_list.append(comp_info)     #adding to main list.
                #print comp_list
        else:
            cat = j.text
            comp_info = {}
            print cat,'http://www.moneycontrol.com/stocks/marketinfo/marketcap/bse/'+j['value']+'.html'
            try:
                in_cat_html = requests.get('http://www.moneycontrol.com/stocks/marketinfo/marketcap/bse/'+j['value']+'.html')
                in_cat_text = BeautifulSoup(in_cat_html.text, 'html.parser')
                td_comp = in_cat_text.find_all('td',{'width':'25%'})
                for k in td_comp:
                    comp_info = {} #for company info.
                    temp = [] #for address.
                    a = k.find('a')
                    comp_rank = comp_rank + 1
                    comp_info['rank'] = comp_rank
                    comp_info['name'] = a.text
                    comp_info['catagory'] = cat
                    #print comp_info
                    html_inlink = requests.get(mainUrl+a['href'])       #going into the company_info button link.
                    text_inlink = BeautifulSoup(html_inlink.text,'html.parser')
                    dt = text_inlink.find('dt', string='COMPANY INFO')      #finding the sublink.
                    a_in = dt.find('a')
                    #print "Going into URL:"+mainUrl+a_in['href']
                    comp_facts = requests.get(mainUrl+a_in['href'])         #going into the sublink.
                    comp_facts_text = BeautifulSoup(comp_facts.text,'html.parser')
                    div = comp_facts_text.find('div',class_='FL b-12 PA5')
                    #print div
                    try:
                        div_lst = div.text.split('\n')
                    except:
                        comp_list.append(comp_info)     #because giving error on rank = 54.
                        continue
                    #print div_lst
                    #print div_lst[1]
                    for z in div_lst[2:]:
                        if(z.replace('\n','').replace('\t','') == ''):
                            continue
                        else:
                            #print z.replace('\n','').replace('\t','')
                            if(z.replace('\n','').replace('\t','').find('Tel: ') != -1):
                                comp_info['telephone'] = z.replace('\n','').replace('\t','').replace('Tel: ','')
                            elif(z.replace('\n','').replace('\t','').find('Fax:') != -1):
                                comp_info['fax'] = z.replace('\n','').replace('\t','').replace('Fax: ','')
                            elif(z.replace('\n','').replace('\t','').find('Email:') != -1):
                                comp_info['email'] = z.replace('\n','').replace('\t','').replace('Email: ','')
                            elif(z.replace('\n','').replace('\t','').find('Website:') != -1):
                                comp_info['website'] = z.replace('\n','').replace('\t','').replace('Website: ','')
                            elif(z.replace('\n','').replace('\t','').find('Group:') != -1):
                                comp_info['group'] = z.replace('\n','').replace('\t','').replace('Group: ','')
                            else:
                                if(z.replace('\n','').replace('\t','').find(',') == -1):
                                    temp_coma = None
                                    temp_coma = z.replace('\n','').replace('\t','')
                                    coma = temp_coma+','
                                    temp.append(coma)
                                else:
                                    temp.append(z.replace('\n','').replace('\t',''))
                    comp_info['registered address'] = ''.join(temp)

                    table4 = comp_facts_text.find('div',class_='boxBg1 table4')
                    table_lst = []
                    for k in table4.find_all('tr'):
                        for l in k.find_all('td',class_='b-12 PT5'):
                                table_lst.append(l.text)
                    temp_gen = gen(table_lst)
                    while True:
                        try:
                            value = temp_gen.next()
                            key = temp_gen.next()
                            if key in comp_info.keys():
                                comp_info[key].append(value)
                            else:
                                comp_info[key]=[value]
                        except:
                            break
                    for k in comp_info.keys():
                        if isinstance(comp_info[k],list):
                            value_str = ','.join(comp_info[k])
                            comp_info[k]=value_str
                    f.write('Done for : '+str(comp_rank)+'\n')       #writing to log file.
                    print comp_info
                    comp_list.append(comp_info)     #adding to main list.
                    #print comp_list
                f.write('Done For Catagory : '+cat+'\n')
            except:
                f.write('Finished  at : '+time.asctime( time.localtime(time.time()) )+' : Error in finding the Companies List in Catagory :'+cat+'\n')
                exit()

df = pd.DataFrame.from_dict(comp_list)
#print '\n'
#print df
df.to_excel('/media/mohitsahni/DATAPOOL-150/QunattaAnalytics/project.02.Nov-11/top100.xlsx')

f.write('finished at : '+time.asctime( time.localtime(time.time()) )+'\n')
f.close()
